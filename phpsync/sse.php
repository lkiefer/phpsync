<?php
header('Content-Type: text/event-stream');
header('Cache-Control: no-cache');

ob_end_flush();
$prev='';
while (true) {
    if(connection_aborted()){
        exit();
    }

    $data = apcu_fetch('phpsync');
    if (
        $data!=$prev
    ){
        echo $data;
        $prev=$data;
    }

    flush();
    usleep(200000);
}
?>
