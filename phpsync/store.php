<?php
require "./password.php";
if (password_verify($_GET['passwd'], $password)){
    // Do not modify state if only the password is sent
    if ($_GET['h'] != ''){
        $h = $_GET['h']=='undefined'?$_GET['h']:(int)$_GET['h'];
        $v = $_GET['v']=='undefined'?$_GET['v']:(int)$_GET['v'];
        $f = $_GET['f']=='undefined'?$_GET['f']:(int)$_GET['f'];
        $p = $_GET['p']=='true'?'true':'false';
        $o = $_GET['o']=='true'?'true':'false';
        apcu_store(
            'phpsync',
            "data: {".
            "\"indexh\": \"$h\",".
            " \"indexv\": \"$v\",".
            " \"indexf\": \"$f\",".
            " \"paused\": \"$p\",".
            " \"overview\": \"$o\"}\n\n",
            60*15
        );
    }
    echo "Password ok";
} else{
    // Reply with the hash of the password, so that the user can put it in password.php
    echo "Bad password. Hash:<br>".password_hash($_GET['passwd'], PASSWORD_DEFAULT);
}
?>
