(function(){
	'use strict';
	var state;

	// Add a new slide to enter a password
	let slide = document.createElement("section");
	let passwd = document.createElement("input");
	let btnSend = document.createElement("input");
	btnSend.setAttribute('type','button');
	btnSend.setAttribute('value','send');
	btnSend.addEventListener("click", sendPasswd);
	let btnContinue = document.createElement("input");
	btnContinue.setAttribute('type','button');
	btnContinue.setAttribute('value','Continue the presentation');
	btnContinue.addEventListener("click", setState);
	passwd.setAttribute('type','password');
	let title = document.createElement("h2");
	title.appendChild(document.createTextNode('phpsync plugin'));
	let label = document.createElement("p");
	label.appendChild(document.createTextNode('Enter the password to control the slideshow'));
	let message = document.createElement("pre");
	slide.appendChild(title);
	slide.appendChild(label);
	slide.appendChild(passwd);
	slide.appendChild(btnSend);
	slide.appendChild(document.createElement('br'));
	slide.appendChild(btnContinue);
	slide.appendChild(message);

	let httpRequest = new XMLHttpRequest();
	httpRequest.onreadystatechange = function() {
		if (httpRequest.readyState === XMLHttpRequest.DONE) {
			if (httpRequest.status === 200) {
				message.innerHTML = httpRequest.responseText;
			} else{
				console.log('error ' + httpRequest.status);
			}
		}
	};

	function sendState(event) {
		// Only send state if the password is set
		if (passwd && passwd.value) {
			state = Reveal.getState();
			let req = 'plugin/phpsync/store.php?h='+state.indexh+'&v='+state.indexv+'&f='+state.indexf+'&p='+state.paused+'&o='+state.overview+'&passwd='+encodeURI(passwd.value);
			httpRequest.open('GET', req, true);
			httpRequest.send();
		}
	}

	function sendPasswd(event) {
		if (passwd && passwd.value) {
			//let state = Reveal.getState();
			let req = 'plugin/phpsync/store.php?passwd='+encodeURI(passwd.value);
			httpRequest.open('GET', req, true);
			httpRequest.send();
		}
	}

	function setState(event) {
		if(state != undefined){
			Reveal.setState(state);
		} else {
			Reveal.slide(0);
		}
	}

	function onReady(event) {
		Reveal.getSlidesElement().appendChild(slide);
		let eventSource = new EventSource("plugin/phpsync/sse.php");
		eventSource.onmessage = function(event) {
			if (passwd && passwd.value==""){
				state = JSON.parse(event.data);
				Reveal.setState(state);
			}
		};
	}

	Reveal.on('slidechanged', sendState);
	Reveal.on('fragmentshown', sendState);
	Reveal.on('fragmenthidden', sendState);
	Reveal.on('overviewshown', sendState);
	Reveal.on('overviewhidden', sendState);
	Reveal.on('paused', sendState);
	Reveal.on('resumed', sendState);
	Reveal.on('ready', onReady);
})();
