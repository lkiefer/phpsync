phpsync
========

phpsync is a reveal.js plugin allowing the synchronization of a slideshow state between several browsers: the attendees will see the same slide at the same time.

Note that such a plugin already exists: multiplex, which requires a Node.js server. This plugin needs a web server with PHP instead.

Requirements
-------------

phpsync requires a web server with PHP installed with the apcu module. Unfortunately, shared web hosting does probably not include this module. This is how to install it on a Debian based system:

`# apt install php-apcu`

How to use it?
---------------

Place the phpsync folder into the reveal.js' plugin folder. Add the plugin to your slideshow by adding this line after the other plugins:

`<script src="plugin/phpsync/phpsync.js"></script>`

Open your slideshow in your browser from your server, and go to the last page. Here enter your password to control the slideshow and click on "send".  
The default password is 'imtheboss'. If you enter an unknown password, the server will send you the corresponding hash. You may use it to modify your password in the "phpsync/password.php" file. Everytime you change the slide, a request is now sent to the server.

Now, everyone having this slideshow open will see the same slide as you.


Limitations
------------

  * The slideshow state is recorded in a unique server wide variable. Then, using this plugin with multiple slideshow at the same time is not supported.
  * On the server side, the state of the slideshow is checked every 0.2s using an infinite loop, then you have a little delay.
  * phpsync use Server Send Events to send the state to browsers. It is not really a "real time" protocol and it may add a little delay.

If these limitations are an issue for you, you may consider using the multiplex plugin.

License
--------

This software is provided under the MIT license. It is provided as is without any warranty. See LICENSE.txt for more informations.
